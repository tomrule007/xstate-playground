import { createMachine, assign } from 'xstate';

// Source: David Khourshid - Introduction to statecharts
// Link: https://youtu.be/_umnF7gpbfg

const initialContext = {
  x: 0,
  y: 0,
  dx: 0,
  dy: 0,
  cursor: 'pointer',
};
const dragDropMachine = createMachine({
  id: 'dragAndDrop',
  initial: 'idle',
  context: initialContext,
  states: {
    idle: {
      on: {
        mousedown: {
          target: 'dragging',
          actions: assign({
            pointerX: (ctx, event) => event.clientX,
            pointerY: (ctx, event) => event.clientY,
            cursor: 'grabbing',
          }),
        },
        reset: {
          actions: assign(initialContext),
        },
      },
    },
    dragging: {
      on: {
        mousemove: {
          actions: assign({
            dx: (ctx, event) => event.clientX - ctx.pointerX,
            dy: (ctx, event) => event.clientY - ctx.pointerY,
          }),
        },
        mouseup: {
          target: 'idle',
          actions: assign({
            x: (ctx, event) => ctx.x + ctx.dx,
            y: (ctx, event) => ctx.y + ctx.dy,
            dx: 0,
            dy: 0,
            pointerX: 0,
            pointerY: 0,
            cursor: 'pointer',
          }),
        },
      },
    },
  },
});

export default dragDropMachine;
