import React from 'react';
import './App.css';
import Between from './component/Between';
import Draggable from './component/Draggable';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>xState Playground</h1>
        <hr />
      </header>
      <main>
        <Between />
        <hr />
        <Between />
        <hr />
        <Draggable />
        <hr />
        <Draggable title="draggable2" />
      </main>
    </div>
  );
}

export default App;
