import { createMachine, interpret, assign } from 'xstate';

// Stateless machine definition
// machine.transition(...) is a pure function used by the interpreter.

const setCtxFrom = (from) => assign({ from: (ctx) => from });

const toggleMachine = createMachine({
  id: 'toggle',
  initial: 'inactive',
  context: {},
  states: {
    inactive: {
      on: { ENABLE: 'between' },
      exit: setCtxFrom('inactive'),
    },
    between: {
      on: {
        CONTINUE: [
          {
            target: 'inactive',
            cond: (context, event) => context.from === 'active',
          },
          {
            target: 'active',
            cond: (context, event) => context.from === 'inactive',
          },
        ],
        BACK: [
          {
            target: 'inactive',
            cond: (context, event) => context.from === 'inactive',
          },
          {
            target: 'active',
            cond: (context, event) => context.from === 'active',
          },
        ],
      },
      exit: setCtxFrom('between'),
    },
    active: { on: { DISABLE: 'between' }, exit: setCtxFrom('active') },
  },
});

// Machine instance with internal state
const toggleService = interpret(toggleMachine, { devTools: true })
  .onTransition((state) => console.log('TRANSISION: ', state.value))
  .start();
// => 'inactive'

export default toggleService;
