import React from 'react';
import toggleService from '../services/betweenService';
import { useService } from '@xstate/react';
const Between = () => {
  const [state, send] = useService(toggleService);

  // Transitions
  const toggle = (from) => send({ type: 'TOGGLE', from: 'test' });

  const isOn = state.value === 'active';
  const isBetween = state.value === 'between';
  const isOff = state.value === 'inactive';
  return (
    <section>
      <h2>betweenService</h2>
      <p> (active, inactive, between)</p>
      <p> Current State: {state.value}</p>
      <button disabled={isBetween || isOn} onClick={() => send('ENABLE')}>
        ON
      </button>
      <button disabled={!isBetween} onClick={() => send('CONTINUE')}>
        Continue
      </button>
      <button disabled={isBetween || isOff} onClick={() => send('DISABLE')}>
        off
      </button>
      <br></br>
      <button disabled={!isBetween} onClick={() => send('BACK')}>
        Back
      </button>
    </section>
  );
};

export default Between;
