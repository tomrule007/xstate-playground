import React, { useEffect } from 'react';
import styles from './Draggable.module.css';
import { useMachine } from '@xstate/react';
import dragDropMachine from '../machines/dragDropMachine';

export default function Draggable({ title = 'Draggable' }) {
  useEffect(() => {
    document.body.addEventListener('mousemove', send);
    return () => {
      document.body.removeEventListener('mousemove', send);
    };
  });

  const [state, send] = useMachine(dragDropMachine);

  const { dx, dy, x, y, cursor } = state.context;

  return (
    <section className={styles.unselectable}>
      <div
        style={{
          '--dx': dx,
          '--dy': dy,
          '--x': x,
          '--y': y,
          cursor,
        }}
        className={styles.draggable}
        onMouseDown={send}
        onMouseUp={send}
      >
        <h2>{title}</h2>
        <p> State: {state.value}</p>
        <button
          onMouseDown={(e) => {
            e.stopPropagation();
            send('reset');
          }}
        >
          reset
        </button>
      </div>
    </section>
  );
}
