# xstate-playground

A small repo for me to playaround with xState.

**xState:** a finite state machine and state chart, library for javascript

Finite: `having limits or bounds` ~ An explicit set of states and there can only be one active.

---

### Finite State Machine

The 5 Properties:

1. one `inital` state
2. a finite number of `states`
3. a finite number of 'events'
4. a mapping of state 'transitions' triggered by events
5. a finite number of `final states`

**WHY:** Use a finite state machine...

- reduce the application complexity. (no longer tracking multiple booleans that makeup one specific state)
- prevent you from getting in impossible states that are hard to debug. (impossible boolean combinations you never planned for users to enter)
- easy to generate flow visualizations and test with zero coupling to the actual interface

---

### Statechart

an extension to finite machines adding useful features to help reduce complexity in problems with large amounts of states.

extra features include..

- Conditions / Guards - conditional checks on event transitions
- hierarchical / nested states
  - parallel / orthogonal states - multiple child states that are independent of each other

---

## Cheatsheet

**Transitions** - (currentState, Event) => nextState

```js
// example
const nextState = someMachine.transition(currentState, 'someEvent');
```

---

**Event** 'stringEvent' -> {type: 'stringEvent'}

can use just string as a shortcut or use type object with additional values as payload

---

**Actions** - only 3 and always fire in this order

1. exit
2. transition
3. entry

---

```js

{
    states:{
        one_state:
    },
    on:{
        ACTION_NAME: '.one_state' // the '.' do an internal transition (will not fire entry/exit actions)
    }
}
// ..
on: {
    EVENT_NAME:
}

```
